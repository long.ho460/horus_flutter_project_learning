import 'package:flutter/material.dart';
import 'package:stream_builder/models/product.dart';

class MyProductItem extends StatelessWidget {
  MyProductItem({Key? key, required this.myItem}) : super(key: key);

  Product myItem;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: Text("Product Detail"),
        ),
      ),
      body: Column(
        children: [
          Center(
            child: Image.network(
              'https://cf.shopee.vn/file/e8057a3aeae649353b879f7204b8136f',
              height: 330.0,
              width: 330.0,
            ),
          ),

          Text(
            myItem.name,
            style: const TextStyle(
              color: Colors.black87,
              fontSize: 24,
              fontWeight: FontWeight.w400,
            ),
          ),
          Text(
            myItem.price.toString() + "đ",
            style: const TextStyle(
              color: Colors.black87,
              fontSize: 24,
              fontWeight: FontWeight.w400,
            ),
          ),
          Text(
            myItem.description,
            style: const TextStyle(
              color: Colors.black87,
              fontSize: 24,
              fontWeight: FontWeight.w400,
            ),
          ),
        ],
      ),
    );
  }
}
