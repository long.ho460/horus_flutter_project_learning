import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:stream_builder/screens/product_item_detail_screen.dart';
import 'package:stream_builder/widgets/product_item.dart';

import '../my_stream_builder.dart';
import '../models/product.dart';
import 'package:flutter/material.dart';

class MyHome extends StatefulWidget {
  MyHome({Key? key}) : super(key: key);

  @override
  State<MyHome> createState() => _MyHomeState();
}

class _MyHomeState extends State<MyHome> {
  ScrollController _scrollController = ScrollController();


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // createLstProducts();
    Timer(const Duration(seconds: 2), (){
      getMoreLstProducts();
    });
    _scrollController.addListener(() {
      if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        print("this is the end of the world");

        Timer(const Duration(seconds: 1), (){
          getMoreLstProducts();
        });
          setState(() {
          // sleep(const Duration(seconds: 2));
        });
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: Text("My Product"),
        ),
      ),
      body: StreamBuilder<List<Product>>(
          stream: getController()!.stream,
          builder: (context, snapshot) {
            print("snap shot length : "  + snapshot.data!.length.toString());
            if(snapshot.hasData){
              print("has data");
              return ListView.separated(
                controller: _scrollController,
                separatorBuilder: (BuildContext context, int index) => const Divider(indent: 20, endIndent: 20,),
                itemCount: snapshot.data!.length + 1, // thêm 1 row để hiển thị loading
                itemBuilder: (context, index) {
                  if(index == snapshot.data!.length){
                    return const CupertinoActivityIndicator();
                  }
                  return InkWell(
                    onTap: (){
                      Route route = MaterialPageRoute(builder: (context) => MyProductItem(myItem: snapshot.data![index]));
                      Navigator.push(context, route);
                      },
                    child: MyProductWidget(myItem: snapshot.data![index]),
                  );
                  // return Text(snapshot.data![index].toString());
                },
              );
            }
            else{
              return const Center(
                child: CupertinoActivityIndicator(),
              );
            }
          }
      ),
    );
  }


}
