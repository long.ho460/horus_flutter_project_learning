import 'package:flutter/material.dart';
import 'package:stream_builder/models/product.dart';


class MyProductWidget extends StatelessWidget {
  MyProductWidget({Key? key, required this.myItem}) : super(key: key);
  Product myItem;

  @override
  Widget build(BuildContext context) {
    return Container(

      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(16.0),
            child: Image.network(
              myItem.imageURL,
              height: 110.0,
              width: 110.0,
            ),
          ),
          // Image.asset('assets/dollar.png', width: 20, height: 20,),
          Container(
            padding:
            const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
            alignment: Alignment.topLeft,
            child: Column(
              children: [
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    myItem.name,
                    style: const TextStyle(
                      color: Colors.black87,
                      fontSize: 24,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    margin: const EdgeInsets.symmetric(vertical: 5.0),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        myItem.description,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(
                          color: Colors.blueGrey,
                          fontSize: 15,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 5.0),
                  child: Align(
                    alignment: Alignment.bottomLeft,
                    child: Row(
                      children: [
                        Image.asset('assets/dollar.png', width: 20, height: 20,),
                        const SizedBox(width: 5,),
                        Text(
                          myItem.price.toString() + "đ",
                          style: const TextStyle(
                            color: Color(0xff287998),
                            fontSize: 18,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            height: 135,
            width: 250,
          ),
        ],
      ),
      // color: Colors.blue,
      height: 125.0,
      margin: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
      padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
    );
  }
}