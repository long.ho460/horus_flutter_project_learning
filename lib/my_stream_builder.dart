import 'dart:async';

import 'package:stream_builder/models/product.dart';
import 'package:stream_builder/widgets/product_item.dart';

// Stream<List<Product>> stream = Stream<List<Product>>.fromIterable(
//   []
// );

StreamController<List<Product>> controller = StreamController();

List<Product> lstProducts = [];

StreamController<List<Product>>? getController() {
  return controller;
}

Stream<List<Product>> getMoreLstProducts() {
  List<Product> lstMyProduct = [];
  lstMyProduct.add(
    Product(
        name: "Kệ Sách",
        description: "Kệ Sách, Hộc Tủ Kệ Để Đồ Bằng Gỗ Nhiều Ngăn Có Ngăn Kéo Tủ Nhỏ Gọn Siêu Tiện Dụng",
        price: 120000,
        imageURL: 'https://cf.shopee.vn/file/e8057a3aeae649353b879f7204b8136f'),
  );
  lstMyProduct.add(
    Product(
        name: "Thẻ nhớ",
        description: "Thẻ Nhớ Micro SD PULUZ 64GB 32GB 16GB Class 10 Thẻ Nhớ Micro Sd 64GB Tốc Độ Cao 80 MB/giây 64GB U3 A1 V30 UHS-I Thẻ Nhớ Microsd TF",
        price: 43378,
        imageURL: 'http://my-test-11.slatic.net/p/cd6c6e997964ff6267511657a4289f67.jpg_720x720q80.jpg_.webp'),
  );
  lstMyProduct.add(
    Product(
        name: "Tai nghe",
        description: "Tai Nghe Chơi Game PULUZ Tai Nghe Game Thủ Tai Nghe Có Dây Âm Thanh Vòm Micro 3.5M Tai Nghe Chơi Game Máy Tính Xách Tay Máy Tính Ánh Sáng Đầy Màu Sắc",
        price: 100000,
        imageURL: 'http://my-test-11.slatic.net/p/72a7847647d6324de9e98b51c00bf6f0.png_720x720q80.jpg_.webp'),
  );
  lstMyProduct.add(
    Product(
        name: "Tủ gỗ",
        description: "Blossom 1:12 Mô Hình Tủ Gỗ Thu Nhỏ Cho Nhà Búp Bê Phụ Kiện Trang Trí Nội Thất",
        price: 88000,
        imageURL: 'http://th-test-11.slatic.net/p/9ff6255d59234bb21033e75ca6bd377f.jpg_720x720q80.jpg_.webp'),
  );

  lstMyProduct.add(
    Product(
        name: "Kệ Sách 2",
        description: "Kệ Sách, Hộc Tủ Kệ Để Đồ Bằng Gỗ Nhiều Ngăn Có Ngăn Kéo Tủ Nhỏ Gọn Siêu Tiện Dụng",
        price: 120000,
        imageURL: 'https://cf.shopee.vn/file/e8057a3aeae649353b879f7204b8136f'),
  );
  lstMyProduct.add(
    Product(
        name: "Thẻ nhớ 2",
        description: "Thẻ Nhớ Micro SD PULUZ 64GB 32GB 16GB Class 10 Thẻ Nhớ Micro Sd 64GB Tốc Độ Cao 80 MB/giây 64GB U3 A1 V30 UHS-I Thẻ Nhớ Microsd TF",
        price: 43378,
        imageURL: 'http://my-test-11.slatic.net/p/cd6c6e997964ff6267511657a4289f67.jpg_720x720q80.jpg_.webp'),
  );
  lstMyProduct.add(
    Product(
        name: "Tai nghe 2",
        description: "Tai Nghe Chơi Game PULUZ Tai Nghe Game Thủ Tai Nghe Có Dây Âm Thanh Vòm Micro 3.5M Tai Nghe Chơi Game Máy Tính Xách Tay Máy Tính Ánh Sáng Đầy Màu Sắc",
        price: 100000,
        imageURL: 'http://my-test-11.slatic.net/p/72a7847647d6324de9e98b51c00bf6f0.png_720x720q80.jpg_.webp'),
  );
  lstMyProduct.add(
    Product(
        name: "Tủ gỗ 2",
        description: "Blossom 1:12 Mô Hình Tủ Gỗ Thu Nhỏ Cho Nhà Búp Bê Phụ Kiện Trang Trí Nội Thất",
        price: 88000,
        imageURL: 'http://th-test-11.slatic.net/p/9ff6255d59234bb21033e75ca6bd377f.jpg_720x720q80.jpg_.webp'),
  );



  lstMyProduct.add(
    Product(
        name: "Kệ Sách 3",
        description: "Kệ Sách, Hộc Tủ Kệ Để Đồ Bằng Gỗ Nhiều Ngăn Có Ngăn Kéo Tủ Nhỏ Gọn Siêu Tiện Dụng",
        price: 120000,
        imageURL: 'https://cf.shopee.vn/file/e8057a3aeae649353b879f7204b8136f'),
  );
  lstMyProduct.add(
    Product(
        name: "Thẻ nhớ 3",
        description: "Thẻ Nhớ Micro SD PULUZ 64GB 32GB 16GB Class 10 Thẻ Nhớ Micro Sd 64GB Tốc Độ Cao 80 MB/giây 64GB U3 A1 V30 UHS-I Thẻ Nhớ Microsd TF",
        price: 43378,
        imageURL: 'http://my-test-11.slatic.net/p/cd6c6e997964ff6267511657a4289f67.jpg_720x720q80.jpg_.webp'),
  );

  // var data = Iterable<int>.generate(10, (value){
  //   // return Product(name: "name $value", description: "description $value", price: value, imageURL: "https://cf.shopee.vn/file/e8057a3aeae649353b879f7204b8136f");
  //   // return lstMyProduct[value];
  //   return value;
  // });
  // return Stream<Product>.periodic(Duration(seconds: 2), (value){
  //   return lstMyProduct[value];
  // }).take(10);

  // stream = Stream<List<Product>>.fromIterable(
  //   <List<Product>>[
  //     // List<Product>.generate(100, (int i) => lstMyProduct[i]),
  //     List<Product>.generate(10, (int i) => Product(
  //         name: "name $i",
  //         description: "description $i",
  //         price: i,
  //         imageURL: "https://cf.shopee.vn/file/e8057a3aeae649353b879f7204b8136f"
  //     )),
  //   ],
  // );

  //   // stream.transform(StreamTransformer.fromHandlers(handleData: (input, sink){
  //   //   for(int i=0; i<10; i++){
  //   //     sink.add(input);
  //   //   }
  //   // }));
  //
  //   // List<Product> lstNewProducts = List<Product>.generate(
  //   //     20,
  //   //     (int i) => Product(
  //   //         name: "name $i",
  //         description: "description $i",
  //         price: i,
  //         imageURL:
  //             "https://cf.shopee.vn/file/e8057a3aeae649353b879f7204b8136f"
  //     ));

  lstProducts = lstProducts + lstMyProduct;

  controller.sink.add(lstProducts);



  return controller.stream;
}
