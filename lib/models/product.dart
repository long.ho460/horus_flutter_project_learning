class Product{
  String name;
  String description;
  int price;
  String imageURL;

  Product({required this.name, required this.description, required this.price, required this.imageURL});

  @override
  String toString() {
    return 'Product{name: $name, description: $description, price: $price, imageURL: $imageURL}';
  }
}